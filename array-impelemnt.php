<?php

class MyArray{

    private $data = [];
    private $length = [];


    public function __construct(){
        $this->length = 0;
        $this->data =  [];
    }

    public function get($index){
        return $this->data[$index];
    }

    public function push($item){
        $this->data[$this->length] = $item;
        $this->length++;
        return $this->data;
    }

    public function pop(){
        $lastItem = $this->data[$this->length - 1];
        array_pop($this->data) ;
        $this->length--;
        return $lastItem;
    }

    public function pushFisrt($index){
        $item = $this->data[$index];
        array_splice($this->data, $item);
        return $item;
    }

    public function deleteAtIndex($index){
        unset($this->data[$index]);
        $this->length--;
        return $this->data;
    }

}


$myArray = new MyArray();
$myArray->push('hlao');
$myArray->push('bayu');
$myArray->push('budi');


// $myArray->pop();
$myArray->deleteAtIndex(1);
var_dump($myArray);